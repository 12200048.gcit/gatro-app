import {StatusBar } from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { phoneValidator } from '../core/helpers/phoneValidator'
import BackButon from '../component/BackButton'

const ResetPasswordScreen = ({navigation}) => {
  const [phone,setPhone]=useState({value:null,error:''})

    const onSubmitPressed=()=>{
      const phoneError=phoneValidator(phone.value)
      if(phoneError){
        setPhone({...phone,error:phoneError})
      }
      else{
        navigation.navigate('OtpVerification')
      }
    }
  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButon goBack={navigation.goBack}/>
      <Logo/>
      <Header>Restore Password</Header>
      <TextInput 
        label="Phone Number"
        value={phone.value}
        error={phone.error}
        errorText={phone.error}
        onChangeText={(text)=>setPhone({value:text,error:""})}
        description="You will receive OTP to above number."
      />
      <Button mode='contained' onPress={onSubmitPressed} >Send</Button>
    </Background>
  )
}

export default ResetPasswordScreen

