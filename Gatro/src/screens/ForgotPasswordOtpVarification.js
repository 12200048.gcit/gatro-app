import {StatusBar } from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { OtpValidator } from '../core/helpers/OtpValidator'
import BackButon from '../component/BackButton'
import Paragraph from '../component/Paragraph'

const ResetPasswordScreen = ({navigation}) => {
  const [otp,setOtp]=useState({value:null,error:''})

    const onVerifyPressed=()=>{
      const otpError=OtpValidator(otp.value)
      if(otpError){
        setOtp({...otp,error:otpError})
      }
    }
  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButon goBack={navigation.goBack}/>
      <Logo/>
      <Header>Verification</Header>
      <Paragraph>We have sent OTP. Enter below to continue</Paragraph>
      <TextInput 
        label="Enter OTP"
        value={otp.value}
        error={otp.error}
        errorText={otp.error}
        onChangeText={(text)=>setOtp({value:text,error:""})}

      />
      <Button mode='contained' onPress={onVerifyPressed} >Verify</Button>
    </Background>
  )
}

export default ResetPasswordScreen
