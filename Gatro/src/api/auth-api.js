import firebase from "firebase/app";
import 'firebase/auth'

export async function signUpUser({name, email, phone, password, confirmPassowrd }){
    try{
        const {user} = await firebase
            .auth
            .createUserWithEmailAndPassword(email, password)
            .createUserWithPhone(phone)

        await firebase.auth().currentUser.updateProfile({
            displayName: name
        })
        return {user}
    }
    catch (error){
       return{
           error: error.message
    }
    }
}