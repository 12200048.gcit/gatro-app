import 'react-native-gesture-handler';
import { StyleSheet, Text, View,Image,Button } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import firebase from 'firebase/app'
import {
  StartScreen,
  LoginScreen,
  SignupScreen,
  ResetPasswordScreen,
  HomeScreen,
  ForgotPasswordOtpVarification
} from './src/screens';
import {firebaseConfig} from './src/core/config'

if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}

const Stack = createNativeStackNavigator();


export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator 
          initialRouteName='StartScreen'
          screenOptions={{headerShown:false}}
        >
          <Stack.Screen name='StartScreen' component={StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='SignupScreen' component={SignupScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen} />
          <Stack.Screen name='HomeScreen' component={HomeScreen} />
          <Stack.Screen name='OtpVerification' component={ForgotPasswordOtpVarification} />
        </Stack.Navigator>

      </NavigationContainer>
    </Provider>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
